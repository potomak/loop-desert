module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      dist: {
        src: ['src/components/**/*.js', 'src/scenes/**/*.js'],
        dest: 'dist/<%= pkg.name %>.js'
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },

    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        },
        debug: true
      }
    },

    watch: {
      files: ['<%= jshint.files %>', 'public/**', '../src/Crafty/crafty*.js'],
      tasks: ['default']
    },

    copy: {
      main: {
        files: [
          {
            src: 'src/game.js',
            dest: 'dist/game.js'
          },
          {
            expand: true,
            src: ['lib/**'],
            dest: 'dist/'
          },
          {
            expand: true,
            cwd: '../../gowar/milka/src/Crafty/',
            src: 'crafty*.js',
            dest: 'dist/lib/'
          },
          {
            expand: true,
            cwd: 'public/',
            src: ['**'],
            dest: 'dist/'
          }
        ]
      }
    },

    clean: ['dist'],

    touch: {
      src: ['dist/.gitkeep'],
    },

    aws: grunt.file.readJSON('aws.json'),
    s3: {
      options: {
        key: '<%= aws.key %>',
        secret: '<%= aws.secret %>',
        bucket: '<%= aws.bucket %>',
        region: '<%= aws.region %>',
        access: 'public-read',
        headers: {
          // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
          'Cache-Control': 'max-age=630720000, public',
          'Expires': new Date(Date.now() + 63072000000).toUTCString()
        }
      },
      dev: {
        // These options override the defaults
        options: {
          encodePaths: true,
          maxOperations: 20
        },
        upload: [
          {
            src: 'dist/**',
            dest: '',
            rel: 'dist',
            options: { gzip: true }
          }
        ]
      }
    },

    connect: {
      server: {
        options: {
          port: 9001,
          base: 'dist',
          keepalive: true,
          hostname: '*'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-s3');
  grunt.loadNpmTasks('grunt-touch');

  grunt.registerTask('test', ['jshint', 'qunit']);

  // grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
  grunt.registerTask('default', ['clean', 'touch', 'jshint', 'concat', 'uglify', 'copy']);

};
