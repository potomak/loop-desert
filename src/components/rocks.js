Crafty.c('Rock', {
  init: function() {
    this.requires('Item')
      .bind('Day', function() {
        this.animate('RockDay', -1);
      })
      .bind('Night', function() {
        this.animate('RockNight', -1);
      });
  }
});

Crafty.c('Rock1', {
  init: function() {
    this.requires('Item, Rock1Sprite')
      .reel('RockDay',   1000, 0, 0, 1)
      .reel('RockNight', 1000, 0, 1, 1)
      .animate('RockDay', -1);

    this.z = -1;
  }
});

Crafty.c('Rock2', {
  init: function() {
    this.requires('Item, Rock2Sprite')
      .reel('RockDay',   1000, 0, 0, 1)
      .reel('RockNight', 1000, 0, 1, 1)
      .animate('RockDay', -1);

    this.z = -2;
    this._item.planeDeepness = 2;
  }
});

Crafty.c('Rock3', {
  init: function() {
    this.requires('Item, Rock3Sprite')
      .reel('RockDay',   1000, 0, 0, 1)
      .reel('RockNight', 1000, 0, 1, 1)
      .animate('RockDay', -1);

    this.z = -2;
    this._item.planeDeepness = 2;
  }
});

Crafty.c('Rock4', {
  init: function() {
    this.requires('Item, Rock4Sprite')
      .reel('RockDay',   1000, 0, 0, 1)
      .reel('RockNight', 1000, 0, 1, 1)
      .animate('RockDay', -1);

    this.z = -2;
    this._item.planeDeepness = 2;
  }
});