Crafty.c('Cactus', {
  init: function() {
    this.requires('Item, CactusSprite')
      .attr({ z: 1 })
      .reel('Cactus', 1000, 0, 0, 1)
      .animate('Cactus', -1);

    this._item.planeDeepness = 0.5;
  }
});