Crafty.c('City', {
  init: function() {
    this.requires('Item')
      .bind('Day', function() {
        this.animate('CityDay', -1);
      })
      .bind('Night', function() {
        this.animate('CityNight', -1);
      });
  }
});

Crafty.c('City1', {
  init: function() {
    this.requires('City, City1Sprite')
      .reel('CityDay',   1000, 0, 0, 1)
      .reel('CityNight', 1000, 0, 1, 1)
      .animate('CityDay', -1);

    this.z = -1;
    this._item.planeDeepness = 1;
  }
});

Crafty.c('City2', {
  init: function() {
    this.requires('City, City2Sprite')
      .reel('CityDay',   1000, 0, 0, 1)
      .reel('CityNight', 1000, 0, 1, 1)
      .animate('CityDay', -1);

    this.z = -2;
    this._item.planeDeepness = 2;
  }
});