Crafty.c('Planet', {
  init: function() {
    this.requires('Actor, SpriteAnimation')
      .attr({
        z: -9,
        _planet: {
          // speeds in radians/frame
          standSpeed: Math.PI/Game.hydrationLevel, // a complete revolution in 72 seconds (a frame is 20 ms)
          offsetX: Game.width/2,
          offsetY: Game.height - Game.floorHeight,
          angle: 0,
          radius: 350
        }
      })
      .bind('EnterFrame', this._planetEnterFrame)
      .bind('PlayerStand', function() {
        this._planet.angularSpeed = this._planet.standSpeed;
      })
      .bind('PlayerWalkEast', function() {
        this._planet.angularSpeed = this._planet.walkEastSpeed;
      })
      .bind('PlayerWalkWest', function() {
        this._planet.angularSpeed = this._planet.walkWestSpeed;
      });

    this._planet.walkEastSpeed = this._planet.standSpeed * 2;
    this._planet.walkWestSpeed = this._planet.standSpeed / 2;
    this._planet.angularSpeed = this._planet.standSpeed;
  },

  _planetEnterFrame: function(data) {
    this._planet.angle += this._planet.angularSpeed;
    if (this._planet.angle > Math.PI*2) {
      this._planet.angle = 0;
    }
    this.x = Math.cos(this._planet.angle) * this._planet.radius + this._planet.offsetX - this.w/2;
    this.y = -1 * Math.sin(this._planet.angle) * this._planet.radius + this._planet.offsetY - this.h/2;
  }
});