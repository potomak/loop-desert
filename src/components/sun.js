Crafty.c('Sun', {
  init: function() {
    this.requires('Planet, SunSprite')
      .reel('Shining', 1000, 0, 0, 2)
      .animate('Shining', -1)
      .bind('EnterFrame', this._sunEnterFrame);

    this._planet.angle = 0;
  },

  _sunEnterFrame: function(data) {
    if (this._planet.angle > 0 && this._planet.angle <= Math.PI) {
      if (Game.dayStatus != 'day') {
        Crafty.trigger('Day');
        Game.dayStatus = 'day';
      }
    } else {
      if (Game.dayStatus != 'night') {
        Crafty.trigger('Night');
        Game.dayStatus = 'night';
      }
    }
  }
});