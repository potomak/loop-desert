Crafty.c('Bottle', {
  init: function() {
    this.requires('Item, BottleSprite, Collision')
      .reel('Bottle', 1000, 0, 0, 1)
      .animate('Bottle', -1)
      .onHit('Player', this._onHit);

    this.z = 0;
    this._item.planeDeepness = 1;
  },

  _onHit: function() {
    Crafty.trigger('BottleHit');
    this.destroy();
  }
});