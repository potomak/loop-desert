Crafty.c('Star', {
  init: function() {
    this.requires('Actor, Color, Tween')
      .attr({ z: -10, w: 4, h: 4, alpha: 0.0 })
      .color('#dedede')
      .bind('Day', function() {
        this.tween({ alpha: 0.0 }, Game.tweenTime);
      })
      .bind('Night', function() {
        this.tween({ alpha: 1.0 }, Game.tweenTime);
      });
  }
});