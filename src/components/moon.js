Crafty.c('Moon', {
  init: function() {
    this.requires('Planet, MoonSprite')
      .reel('Shining', 1000, 0, 0, 1)
      .animate('Shining', -1);

    this._planet.angle = Math.PI;
  }
});