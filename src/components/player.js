Crafty.c('Player', {
  init: function() {
    this.requires('Actor, SpriteAnimation, PlayerSprite')
      .attr({
        walking: false,
        hydration: Game.hydrationLevel,
        alive: true,
        direction: 1, // 1 = west, -1 = east
        distance: 0,
        choke: false
      })
      .reel('StandingEast', 1000, 0, 0, 1)
      .reel('StandingWest', 1000, 0, 1, 1)
      .reel('WalkingEast',  1000, 0, 2, 7)
      .reel('WalkingWest',  1000, 0, 3, 7)
      .reel('DyingEast',    1000, 0, 4, 5)
      .reel('DyingWest',    1000, 0, 5, 5)
      .reel('Winning',      1000, 0, 6, 4)
      .animate('StandingEast', -1)
      .bind('KeyDown', this._onKeyDown)
      .bind('KeyUp', this._onKeyUp)
      .bind('EnterFrame', this._onEnterFrame)
      .bind('Death', this._onDeath)
      .bind('Win', this._onWin)
      .bind('BottleHit', this._onBoottleHit);
  },

  _onKeyDown: function(e) {
    if (!this.walking) {
      if (e.key == Crafty.keys.LEFT_ARROW || e.key == Crafty.keys.RIGHT_ARROW) {
        this.walking = true;
        Crafty.audio.play('walk', -1, Game.effectsVolume);
      }

      if (e.key == Crafty.keys.LEFT_ARROW) {
        this.direction = 1;
        this.animate('WalkingWest', -1);
        Crafty.trigger('PlayerWalkWest');
      } else if (e.key == Crafty.keys.RIGHT_ARROW) {
        this.direction = -1;
        this.animate('WalkingEast', -1);
        Crafty.trigger('PlayerWalkEast');
      }
    }
  },

  _onKeyUp: function(e) {
    if (this.walking) {
      if (e.key == Crafty.keys.LEFT_ARROW || e.key == Crafty.keys.RIGHT_ARROW) {
        this.walking = false;
        Crafty.audio.stop('walk');
        Crafty.trigger('PlayerStand');
      }

      if (e.key == Crafty.keys.LEFT_ARROW) {
        this.animate('StandingWest', -1);
      } else if (e.key == Crafty.keys.RIGHT_ARROW) {
        this.animate('StandingEast', -1);
      }
    }
  },

  _onEnterFrame: function(data) {
    if (this.alive) {
      if (Game.dayStatus == 'day') {
        this.hydration -= 1;
        Crafty.trigger('HydrationLevel', Game.hydrationWidth*this.hydration/Game.hydrationLevel);
      }

      if (this.hydration <= 0) {
        Crafty.trigger('Death');
      }

      if (!this.choke && this.hydration <= 415) {
        this.choke = true;
        Crafty.audio.play('choke', 1, Game.effectsVolume);
      }

      if (this.walking) {
        this.distance += this.direction;

        if (this.distance >= 4500) {
          Crafty.trigger('Win');
        }
      }
    }
  },

  _onDeath: function() {
    this.alive = false;
    this.walking = false;
    Crafty.audio.stop('walk');
    Crafty.audio.play('death_breath', 1, Game.effectsVolume);

    this._removeListeners();

    Game.log(this.distance);

    if (this.direction == 1) {
      this.animate('DyingWest', 1);
    } else {
      this.animate('DyingEast', 1);
    }
  },

  _onWin: function() {
    this.walking = false;
    Crafty.audio.stop('walk');
    Crafty.audio.stop('choke');
    Crafty.audio.play('wohoo', 1, Game.effectsVolume);

    this._removeListeners();

    Game.log(this.distance);

    this.animate('Winning', -1);
  },

  _removeListeners: function() {
    this.unbind('KeyDown', this._onKeyDown);
    this.unbind('KeyUp', this._onKeyUp);
    this.unbind('EnterFrame', this._onEnterFrame);
    this.unbind('Death', this._onDeath);
    this.unbind('Win', this._onWin);
  },

  _onBoottleHit: function() {
    this.choke = false;
    Crafty.audio.stop('choke');
    Crafty.audio.play('drink', 1, Game.effectsVolume);
    this.hydration = Game.hydrationLevel;
    Crafty.trigger('HydrationLevel', Game.hydrationWidth*this.hydration/Game.hydrationLevel);
  }
});