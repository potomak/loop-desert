Crafty.c('ColorTween', {
  init: function() {
    this.requires('Color, Tween')
      .attr({
        _bg: {
          dayColor: {r: 0, g: 0, b: 0},
          nightColor: {r: 0, g: 0, b: 0}
        },
        _bgR: 0,
        _bgG: 0,
        _bgB: 0,
      })
      .bind('Day', function() {
        this.tween({
          _bgR: this._bg.dayColor.r,
          _bgG: this._bg.dayColor.g,
          _bgB: this._bg.dayColor.b
        }, 4000);
      })
      .bind('Night', function() {
        this.tween({
          _bgR: this._bg.nightColor.r,
          _bgG: this._bg.nightColor.g,
          _bgB: this._bg.nightColor.b
        }, 4000);
      })
      .bind('EnterFrame', function() {
        this.color(Game.rgbToString({ r: this._bgR, g: this._bgG, b: this._bgB }));
      });
  }
});

Crafty.c('Sky', {
  init: function() {
    this.requires('Actor, ColorTween')
      .attr({
        z: -11,
        w: Game.width,
        h: Game.height - Game.floorHeight,
        _bg: {
          dayColor: Game.daySkyColor,
          nightColor: Game.nightSkyColor
        },
        _bgR: Game.daySkyColor.r,
        _bgG: Game.daySkyColor.g,
        _bgB: Game.daySkyColor.b,
      })
      .color(Game.rgbToString(Game.daySkyColor));
  }
});

Crafty.c('Floor', {
  init: function() {
    this.requires('Actor, ColorTween')
      .attr({
        y: Game.height - Game.floorHeight,
        z: -8,
        w: Game.width,
        h: Game.floorHeight,
        _bg: {
          dayColor: Game.dayFloorColor,
          nightColor: Game.nightFloorColor
        },
        _bgR: Game.dayFloorColor.r,
        _bgG: Game.dayFloorColor.g,
        _bgB: Game.dayFloorColor.b,
      })
      .color(Game.rgbToString(Game.dayFloorColor));
  }
});

Crafty.c('FloorDark', {
  init: function() {
    this.requires('Actor, ColorTween')
      .attr({
        z: -8,
        w: Game.width,
        _bg: {
          dayColor: Game.dayFloorDarkColor,
          nightColor: Game.nightFloorDarkColor
        },
        _bgR: Game.dayFloorDarkColor.r,
        _bgG: Game.dayFloorDarkColor.g,
        _bgB: Game.dayFloorDarkColor.b,
      })
      .color(Game.rgbToString(Game.dayFloorDarkColor));
  }
});