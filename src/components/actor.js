Crafty.c('Actor', {
  init: function() {
    this.requires('2D, Canvas');
  },

  at: function(x, y) {
    this.x = x;
    this.y = y;

    return this;
  }
});