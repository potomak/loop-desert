Crafty.c('Item', {
  init: function() {
    this.requires('Actor, SpriteAnimation')
      .attr({
        _item: {
          planeDeepness: 1,
          speed: 2,         // pixels/frame at planeDeepness == 1
          direction: 0      // 1 = west, -1 = east, 0 = stand
        }
      })
      .bind('EnterFrame', this._itemEnterFrame)
      .bind('PlayerWalkWest', this._itemPlayerWalkWest)
      .bind('PlayerWalkEast', this._itemPlayerWalkEast)
      .bind('PlayerStand', this._itemPlayerStand)
      .bind('Death', this._onGameEnd)
      .bind('Win', this._onGameEnd);
  },

  _itemEnterFrame: function(data) {
    this.x += this._item.direction * this._item.speed/this._item.planeDeepness;
  },

  _itemPlayerWalkEast: function() {
    this._item.direction = -1;
  },

  _itemPlayerWalkWest: function() {
    this._item.direction = 1;
  },

  _itemPlayerStand: function() {
    this._item.direction = 0;
  },

  _onGameEnd: function() {
    this.unbind('EnterFrame', this._itemEnterFrame);
  }
});