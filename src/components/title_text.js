Crafty.c('TitleText', {
  init: function() {
    this.requires('2D, DOM, Text')
      .attr({ w: Game.width, y: 200 })
      .textFont(Game.titleFont)
      .css({ 'text-align': 'center' })
      .unselectable()
      .bind('Day', function() {
        this.textColor('#333333');
      })
      .bind('Night', function() {
        this.textColor('#dedede');
      });

    if (Game.dayStatus == 'day') {
      this.textColor('#333333');
    } else {
      this.textColor('#dedede');
    }
  }
});