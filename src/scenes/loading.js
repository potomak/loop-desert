Crafty.scene('Loading', function() {
  var assets = [
    'assets/sounds/theme.mp3',
    'assets/sounds/theme.m4a',
    'assets/sounds/theme.ogg',
    'assets/sounds/walk.mp3',
    'assets/sounds/walk.m4a',
    'assets/sounds/walk.ogg',
    'assets/sounds/death_breath.mp3',
    'assets/sounds/death_breath.m4a',
    'assets/sounds/death_breath.ogg',
    'assets/sounds/drink.mp3',
    'assets/sounds/drink.m4a',
    'assets/sounds/drink.ogg',
    'assets/sounds/choke.mp3',
    'assets/sounds/choke.m4a',
    'assets/sounds/choke.ogg',
    'assets/sounds/wohoo.mp3',
    'assets/sounds/wohoo.m4a',
    'assets/sounds/wohoo.ogg',
    'assets/player.png',
    'assets/sun.png',
    'assets/moon.png',
    'assets/city_1.png',
    'assets/city_2.png',
    'assets/rock_1.png',
    'assets/rock_2.png',
    'assets/rock_3.png',
    'assets/rock_4.png',
    'assets/cactus.png',
    'assets/heart.png',
    'assets/bottle.png'
  ];

  this.progressLoading = Crafty.e('2D, ProgressBar')
    .attr({ w: Game.width, h: 30, y: Game.height / 2 })
    .progressBar('LoadingProgress', 10, 100, false, '#fff', '#000', 'DOM');

  Crafty.load(assets, function() {
    Crafty.sprite(128, 128, 'assets/player.png', {
      PlayerSprite: [0, 0],
    });

    Crafty.sprite(128, 128, 'assets/sun.png', {
      SunSprite: [0, 0],
    });

    Crafty.sprite(128, 128, 'assets/moon.png', {
      MoonSprite: [0, 0],
    });

    Crafty.sprite(133, 84, 'assets/rock_1.png', {
      Rock1Sprite: [0, 0],
    });

    Crafty.sprite(22, 36, 'assets/rock_2.png', {
      Rock2Sprite: [0, 0],
    });

    Crafty.sprite(80, 53, 'assets/rock_3.png', {
      Rock3Sprite: [0, 0],
    });

    Crafty.sprite(46, 34, 'assets/rock_4.png', {
      Rock4Sprite: [0, 0],
    });

    Crafty.sprite(88, 184, 'assets/cactus.png', {
      CactusSprite: [0, 0],
    });

    Crafty.sprite(32, 32, 'assets/bottle.png', {
      BottleSprite: [0, 0],
    });

    Crafty.sprite(800, 367, 'assets/city_1.png', {
      City1Sprite: [0, 0],
    });

    Crafty.sprite(800, 343, 'assets/city_2.png', {
      City2Sprite: [0, 0],
    });

    // audio samples
    Crafty.audio.add({
      theme: [
        'assets/sounds/theme.mp3',
        'assets/sounds/theme.m4a',
        'assets/sounds/theme.ogg'
      ],
      walk: [
        'assets/sounds/walk.mp3',
        'assets/sounds/walk.m4a',
        'assets/sounds/walk.ogg'
      ],
      death_breath: [
        'assets/sounds/death_breath.mp3',
        'assets/sounds/death_breath.m4a',
        'assets/sounds/death_breath.ogg'
      ],
      drink: [
        'assets/sounds/drink.mp3',
        'assets/sounds/drink.m4a',
        'assets/sounds/drink.ogg'
      ],
      choke: [
        'assets/sounds/choke.mp3',
        'assets/sounds/choke.m4a',
        'assets/sounds/choke.ogg'
      ],
      wohoo: [
        'assets/sounds/wohoo.mp3',
        'assets/sounds/wohoo.m4a',
        'assets/sounds/wohoo.ogg'
      ]
    });

    Crafty.scene('Game');
  }, function (e) {
    Crafty.trigger('LoadingProgress', e.percent);
  });
});
