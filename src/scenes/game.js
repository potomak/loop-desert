Crafty.scene('Game', function() {
  // play theme if not already playing
  if (Crafty.audio.sounds.theme.played === 0) {
    Crafty.audio.play('theme', -1, Game.musicVolume);
  }

  // sky
  this.sky = Crafty.e('Sky');
  // sun
  this.sun = Crafty.e('Sun');
  // moon
  this.moon = Crafty.e('Moon');
  // dark floors
  this.darkFloor = [];
  this.darkFloor.push(Crafty.e('FloorDark').attr({ y: Game.height - Game.floorHeight, h: 20 }));
  this.darkFloor.push(Crafty.e('FloorDark').attr({ y: (Game.height - Game.floorHeight) + 30, h: 5 }));
  // floor
  this.floor = Crafty.e('Floor');

  // stars
  this.stars = [];
  this.stars.push(Crafty.e('Star').at(161, 102));
  this.stars.push(Crafty.e('Star').at(279, 207));
  this.stars.push(Crafty.e('Star').at(374, 61));
  this.stars.push(Crafty.e('Star').at(597, 133));
  this.stars.push(Crafty.e('Star').at(782, 243));
  this.stars.push(Crafty.e('Star').at(613, 348));
  this.stars.push(Crafty.e('Star').at(409, 397));
  this.stars.push(Crafty.e('Star').at(310, 323));
  this.stars.push(Crafty.e('Star').at(155, 341));
  this.stars.push(Crafty.e('Star').at(237, 452));
  this.stars.push(Crafty.e('Star').at(817, 90));
  this.stars.push(Crafty.e('Star').at(109, 269));
  this.stars.push(Crafty.e('Star').at(557, 273));

  // items
  this.items = [];
  this.items.push(Crafty.e('Rock4').at(200, 445));
  this.items.push(Crafty.e('Rock4').at(1200, 445));
  this.items.push(Crafty.e('Rock4').at(2200, 445));
  this.items.push(Crafty.e('Rock4').at(-1200, 445));
  this.items.push(Crafty.e('Rock3').at(750, 420));
  this.items.push(Crafty.e('Rock3').at(1550, 420));
  this.items.push(Crafty.e('Rock3').at(-750, 420));
  this.items.push(Crafty.e('Rock3').at(-1650, 420));
  this.items.push(Crafty.e('Rock2').at(-200, 450));
  this.items.push(Crafty.e('Rock2').at(-1900, 450));
  this.items.push(Crafty.e('Rock2').at(-3200, 450));
  this.items.push(Crafty.e('Rock2').at(-3800, 450));
  this.items.push(Crafty.e('Rock1').at(500, 410));
  this.items.push(Crafty.e('Rock1').at(2500, 410));
  this.items.push(Crafty.e('Rock1').at(4500, 410));
  this.items.push(Crafty.e('Rock1').at(-2500, 410));
  this.items.push(Crafty.e('Rock1').at(-5500, 410));
  this.items.push(Crafty.e('Rock1').at(-7500, 410));
  this.items.push(Crafty.e('Cactus').at(200, 380));
  this.items.push(Crafty.e('Cactus').at(4200, 380));
  this.items.push(Crafty.e('Cactus').at(10000, 380));
  this.items.push(Crafty.e('Cactus').at(-4200, 380));
  this.items.push(Crafty.e('Cactus').at(-8200, 380));
  this.items.push(Crafty.e('Cactus').at(-10200, 380));
  this.items.push(Crafty.e('Cactus').at(-12200, 380));
  this.items.push(Crafty.e('Cactus').at(-16200, 380));

  // bottles
  this.bottles = [];
  this.bottles.push(Crafty.e('Bottle').at(6000, 500));
  this.bottles.push(Crafty.e('Bottle').at(-3400, 500));

  // city
  this.city = [];
  this.city.push(Crafty.e('City1').at(-9200, 140));
  this.city.push(Crafty.e('City2').at(-4750, 130));

  // player
  this.player = Crafty.e('Player').at(Game.width/2 - 64, 400);

  // heart
  this.heart = Crafty.e('Actor, Image')
    .image('assets/heart.png')
    .at(10, 10);
  // hydration
  this.hydrationLevel = Crafty.e('2D, ProgressBar')
    .attr({ x: 52, y: 12, w: Game.hydrationWidth, h: 32 })
    .progressBar('HydrationLevel', Game.hydrationWidth, Game.hydrationWidth, false, '#ecf0f1', '#e74c3c', 'Canvas');

  this.playing = true;

  this.removeOSD = function() {
    this.heart.destroy();
    this.hydrationLevel.y = -100;
  };

  this.onPlayerDeath = function() {
    this.removeOSD();
    this.playing = false;

    Crafty.e('TitleText').text('Press R to restart');
  };

  this.onPlayerWin = function() {
    this.removeOSD();
    this.playing = false;

    Crafty.e('TitleText')
      .attr({ y: 160 })
      .textFont({ 'size': '128px' })
      .text('Loop Desert');

    Crafty.e('TitleText')
      .attr({ y: 260 })
      .text('by Giovanni Cappellotto');
  };

  this.onKeyDown = function(e) {
    if (!this.playing && e.keyCode == Crafty.keys.R) {
      Crafty.scene('Game');
    }
  };

  this.bind('Death', this.onPlayerDeath);
  this.bind('Win', this.onPlayerWin);
  Crafty.addEvent(this, 'keydown', this.onKeyDown);
}, function() {
  this.unbind('Death', this.onPlayerDeath);
  this.unbind('Win', this.onPlayerWin);
  Crafty.removeEvent(this, 'keydown', this.onKeyDown);
});
