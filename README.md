# Loop Desert

Game source files, libraries, HTML files and assets are *transpiled* and copied into the `dist` using grunt.

To install grunt see [Grunt getting started guide](http://gruntjs.com/getting-started).

The game uses a version of the Crafty.js engine hosted at http://github.com/potomak/Crafty.

*Note:* After each npm packages installed, run `[sudo] npm install`

Workflow:

1. run `grunt` default task
1. run `grunt watch` task to rerun default task at file changes

In another shell run `grunt connect` to spawn a local server serving the content of the `dist` directory.

*Note:* to rebuild Crafty.js library run `grunt build:dev` from `src/Crafty`.